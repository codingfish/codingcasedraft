<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $productUserSubscription;

    /**
     * @ORM\Column(type="integer")
     */
    private $userGroup;

    /**
     * @var CartPosition[]|ArrayCollection
     *
     * @OneToMany(targetEntity="CartPosition", mappedBy="user", cascade={"remove"})
     */
    private $cartPositions;

    /**
     * @var ProductSubscription[]|ArrayCollection
     *
     * @OneToMany(targetEntity="productSubscription", mappedBy="user", cascade={"remove"})
     */
    private $productSubscriptions;

    /**
     * @ORM\Column(type="string")
     */
    private $streetAndNumber;

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $postal;
    /**
     * @ORM\Column(type="string")
     */
    private $password;

    // + generic getters and setters
}
