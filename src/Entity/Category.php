<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Product[]ArrayCollection
     *
     * @ManyToMany(targetEntity="Product", mappedBy="categories")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $parentCategory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Description;

    // + generic getters and setters
}
