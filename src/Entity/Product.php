<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Category[]ArrayCollection
     *
     * @ManyToMany(targetEntity="Category", inversedBy="products")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255, options={"default" = "physical"})
     */
    private $productType;

    /**
     * @var GraduatedPrice[]ArrayCollection
     *
     * @ManyToMany(targetEntity="GraduatedPrice", inversedBy="products")
     */
    private $graduatedPrices;

    /**
     * @ORM\Column(type="integer")
     */
    private $productNumber;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $in_stock;

    /**
     * @var File
     *
     * @ManyToOne(targetEntity="File")
     */
    private $downloadFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default" = "download_link"})
     */
    private $digitalDistributionType;

    /**
     * @var File[]ArrayCollection
     *
     * @ManyToMany(targetEntity="File", mappedBy="productsThatUseFileAsPicture")
     */
    private $pictures;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->graduatedPrices = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    // + generic getters and setters
}
