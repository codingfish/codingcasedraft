<?php

namespace App\Entity;

use App\Repository\GraduatedPriceRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @ORM\Entity(repositoryClass=GraduatedPriceRepository::class)
 */
class GraduatedPrice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, options={"default" : "base_price"})
     */
    private $quantityCondition;


    /**
     * @var Product[]ArrayCollection
     *
     * @ManyToMany(targetEntity="Product", mappedBy="graduatedPrices")
     */
    private $products;

    // + generic getters and setters
}
