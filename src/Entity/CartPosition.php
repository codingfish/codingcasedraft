<?php

namespace App\Entity;

use App\Repository\CartPositionRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=CartPositionRepository::class)
 */
class CartPosition
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @var User
     *
     * @ManyToOne(targetEntity="User", inversedBy="cartPositions")
     */
    private $user;

    /**
     * @var GraduatedPrice
     *
     * @ManyToOne(targetEntity="GraduatedPrice")
     */
    private $appliedPrice;

    /**
     * @var Product
     *
     * @ManyToOne(targetEntity="GraduatedPrice")
     */
    private $product;


    // + generic getters and setters
}
