<?php

namespace App\Entity;

use App\Repository\ProductSubscriptionRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=ProductSubscriptionRepository::class)
 */
class ProductSubscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ManyToOne(targetEntity="User", inversedBy="productSubscriptions")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $subscriptionEventType;

    // + generic getters and setters
}
