<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CartController
 * @package App\Controller
 */
class CartController extends AbstractController
{

    /**
     *
     * Constructor -> Add Dependencies (Service, predefined Responses
     *
     *
     */


    /**
     * @Route("/cart", name="listing")
     */
    public function cart(): Response
    {
        $productRepository = new productRepository();
        $products = $productRepository->findAll();

        return $this->render('listing/index.html.twig', [
            'controller_name' => 'CartController',
            ['products' => $products]
        ]);
    }

    /**
     * @Route("/cart/addProducts", name="addProductsToCart", methods={"POST"}, options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true})
     *
     * @return JSONResponse
     */
    public function addProductsToCart(Request $request): Response
    {
        $requestData = $request->request->all();
        $productId = (int)$requestData['productId'];
        $quantity = (int)$requestData['quantity'];

        try {
            $this->cartService->addProducts($productId, $quantity);
        } catch ($exception) {
            return new ErrorResponse();
        }

        return new successResponse();
    }

    /**
     * @Route("/cart/removeProducts", name="removeProductsFromCart", methods={"POST"}, options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true})
     *
     * @return JSONResponse
     */
    public function removeProductsFromCart(Request $request): Response
    {
        $requestData = $request->request->all();
        $productId = (int)$requestData['productId'];
        $quantity = (int)$requestData['quantity'];

        try {
            $this->cartService->removeProducts($productId, $quantity);
        } catch ($exception) {
            return new ErrorResponse();
        }

        return new successResponse();
    }

    /**
     * @Route("/cart/buy", name="buy", methods={"GET"}, options={"seo"="false"}, methods={"POST"}, defaults={"XmlHttpRequest"=true})
     *
     * @return JSONResponse
     */
    public function buy(Request $request): Response {
        try {
            $this->mailService->createInvoice();
            $this->cartService->flush();
        } catch ($exception) {
            return new ErrorResponse();
        }

        return new successResponse();
    }
}
